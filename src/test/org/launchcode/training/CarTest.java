package org.launchcode.training;

import static org.junit.Assert.*;
import org.junit.Test;

public class CarTest {


    @Test
    public void gasTankLevelDefaulted(){
        Car car = new Car("Toyota", "Prius", 10, 50);
        assertEquals( "10.0", Double.toString(car.getGasTankLevel()));
    }

    @Test
    public void gasTankLevelAccurateAfterDriving() {
        Car car = new Car("Toyota", "Prius", 10, 50);
        car.drive(250);
        assertEquals(5.0, car.getGasTankLevel(), .01);
    }

    @Test
    public void gasTankLevelAccurateAfterPastRange() {
        Car car = new Car("Toyota", "Prius", 10, 50);
        car.drive(501);
        assertEquals(0.0, car.getGasTankLevel(), .01);
    }
    //TODO: can't have more gas than tank size, expect an exception
    @Test(expected = IllegalArgumentException.class)
    public void tryToOverFillTank() {
        Car car = new Car("Toyota", "Prius", 10, 50);

        car.setGasTankLevel(12);
        fail("Should not get here; too much gas exception");

    }
}
